Rails.application.routes.draw do

  namespace :v1 do

    # Authentication
    namespace :auth do
      # signin
      get '/signin', to: 'sessions#signin'
      # signout
      get '/signout', to: 'sessions#signout'
      # register
      get '/signup', to: 'registration#new'
      # reset password 
      get '/reset_password', to: 'registration#reset_password'
      # forgot username require email or phone number
      get '/forgot_username', to: 'registration#forgot_username'
      # forgot email require username or phone number
      get '/forgot_email', to: 'registration#forgot_email'
    end

    # Administration
    namespace :admin do
      # dashboard
      # all user's
      # approved user's
      # user's pendding approval
      namespace :analytics do
        # basic analytics 
        # new users
        # inactive users
        # active users
        # suspended users
      end
    end


    # Transaction
    namespace :transaction do
    end


  end # V1 routs 

  # handle request errors here
  # page not found

  get '/404', to: 'erros#not_found'


end
