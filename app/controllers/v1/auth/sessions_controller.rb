class V1::Auth::SessionsController < ApplicationController
    # handle login and log out 

    def signin
        render json: 'signin'
    end

    def signout
        render json: 'signout'
    end

end