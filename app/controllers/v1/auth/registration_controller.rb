class V1::Auth::RegistrationController < ApplicationController
    
    def new
        # register new account
        render json: "register new user #{Time.now}"
        #new user registration
    end

    def reset_password
        # reset password 
        # email or username required
        render json: "reset password #{Time.now}"
    end

    def forgot_username
        # forgot username require email or phone number
        render json: "forgot suername #{Time.now}"
    end

    def forgot_email
        # forgot email require username or phone number
        render json: "forgot email #{Time.now}"
    end
    
end