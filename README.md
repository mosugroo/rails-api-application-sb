# Rails API application
Smart Bank Transaction API

## How to run the Rails Application
To install all dependencies run `bundle install`

Create the database table and run the migration run
`rails db:create db:migrate`

Lastly you need to set up the following environment variables
#### Environment veriables
* `SECRETE_KEY_BASE`
* `DATABASE_HOST`
* `DATABASE_PORT`
* `DATABASE_USERNAME`
* `DATABASE_PASSWORD`
* `RAILS_MAX_THREADS` Default 5

#### Start the application "local"
Run `rails s`

## How to run the docker container
<!-- documentation for docker containers -->
##### Build the application docker image
`docker build -t rails_api .`

to run build arg ENV add the following code to Dockerfile
```
    ARG ENV
    RUN if [ "$ENV" = "production" ] ; then CMD ["rails", "server","-e", "production","-b", "0.0.0.0","-p","3000"]; else CMD ["rails", "server","-b", "0.0.0.0","-p","3000"] ; fi
```
to build the compressed version 

``
    docker build --compress -t rails_api . --build-arg ENV=production
``
##### Run the application docker image
`docker run --name rails_api_container rails_api -p 3000:300`
##### Pull postgres image form registry
`docker pull postgres`
##### Run the postgres container 
`docker run --name database_postgres -p 5432:5432 -e POSTGRES_PASSWORD=mysecretpassword -d postgres`